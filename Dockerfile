FROM python:3.8.13-slim-buster

# tools
RUN apt-get update && apt-get install -y curl iputils-ping

ENV APP_HOME /src
RUN mkdir $APP_HOME

# set work directory
WORKDIR $APP_HOME

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependencies
RUN pip install --upgrade pip
COPY ./requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

