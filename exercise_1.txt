<app>
    <integer name="num"/>
    <integer name="temp"/>
    <integer name="i" initial_value="1"/>
  
    <func signal="main">
    	<print>Enter an integer:<print/>
        <read_input>
            <variable name="num"/> 
        <read_input/>
			
        <loop variable="i" value="num">
            <assign variable="temp" value="num"/> 
            <mod variable="temp" value="i" />
            <compare variable="temp" value="0"/>
            <add variable="i" value="1" />
        </loop>
    </func>

    <func signal="signal.equals">
        <print>
            <variable name="i"/>
            is divisor of
            <variable name="num"/>
        </print>
    </func>
</app>