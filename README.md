# README #

This is a solution for a backend test with  2 problems:

- http://tiny.cc/fya5tz (you can find in exercise_1.txt file)
- http://tiny.cc/cya5tz

### How do I get set up? ###

in root directory execute.

`docker-compose up -d`


### How to test? ###
This api application has two endpoint. You can test in this way

1. New Task

- URL: http://localhost:5000/new_task

- method: POST

- input body: {'cmd': 'ls'}

- return {'id': 'xxx'}

2. Get OutPut

- URL: http://localhost:5000/get_output/:id

- method: GET

- return {'output': 'xxx'}
 




