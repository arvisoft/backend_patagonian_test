from flask import Flask
from .serializers import MongoJSONEncoder, ObjectIdConverter
import os
from . celeryconfig import REDIS_HOST, REDIS_PORT

app = Flask(__name__)
app.json_encoder = MongoJSONEncoder
app.url_map.converters['objectid'] = ObjectIdConverter

app.config.update(
    CELERY_BROKER_URL=f"redis://{REDIS_HOST}:{REDIS_PORT}",
    CELERY_RESULT_BACKEND=f"redis://{REDIS_HOST}:{REDIS_PORT}",
)