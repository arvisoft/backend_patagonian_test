from bson import ObjectId
from celery import Celery
from .app import app as flask_app
from .db import get_db
import subprocess
from . import celeryconfig


def make_celery(app):
    celery = Celery(
        app.import_name,
        backend=app.config['CELERY_RESULT_BACKEND'],
        broker=app.config['CELERY_BROKER_URL']
    )
    # celery.conf.update(app.config)
    celery.config_from_object(celeryconfig)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery

celery = make_celery(flask_app)


@celery.task()
def execute_command(cmd, id):
    db = get_db()
    result = subprocess.run(cmd.split(), stdout=subprocess.PIPE)
    obj = db.tasks.update_one({'_id': ObjectId(id)}, {'$set': {'output': result.stdout.decode('UTF-8'), 'status': True}})
    print(f"result cmd update into document {obj.matched_count}")
    return cmd