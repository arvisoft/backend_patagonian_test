from pymongo import MongoClient
import os

def get_db():
    client = MongoClient(
        host = os.getenv('MONGO_HOSTNAME', 'localhost'),
        port = int(os.getenv('MONGO_PORT', 27017)),
        username = os.getenv('MONGO_INITDB_ROOT_USERNAME', 'root'),
        password = os.getenv('MONGO_INITDB_ROOT_PASSWORD', '123456'),
        authSource = os.getenv('MONGO_AUTHSOURCE', 'admin'),
    )
    db = client["flask_db"]
    return db