from .app import app
from flask import jsonify, request
from .db import get_db
from .celerymod import execute_command


@app.route('/new_task', methods=["POST"])
def new_task():
    db = get_db()
    cmd = request.get_json().get("cmd")
    col = db["tasks"]
    obj = col.insert_one(dict(cmd=cmd, status=False, output=''))
    result = execute_command.delay(cmd, str(obj.inserted_id))
    result.wait()
    return jsonify({'id': obj.inserted_id})

@app.route('/get_output/<objectid:id>', methods=["GET"])
def get_output(id):
    db = get_db()
    obj = db.tasks.find_one({'_id': id})
    return jsonify({'output': obj.get('output')})
